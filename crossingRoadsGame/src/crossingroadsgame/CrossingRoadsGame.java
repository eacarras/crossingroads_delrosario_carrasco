
package crossingroadsgame;

import Windows.OptionsWindow;
import javafx.scene.image.ImageView;

/**
 *
 * @author eacarras
 */
public class CrossingRoadsGame {

    
    public static void main(String[] args) {
        // Se llama a la ventana de optionWindow
        OptionsWindow optionwindow = new Windows.OptionsWindow();
        optionwindow.main(args);
    }
    
    public static boolean Choque(ImageView avatar,ImageView obs ) {
        if (avatar.getBoundsInParent().intersects(obs.getBoundsInParent())) {
            return true;          
        }
        return false;
    }
    
}
