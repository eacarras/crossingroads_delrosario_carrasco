
package Windows;

import java.io.File;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 *
 * @author eacarras
 */
public class NewGameWindow extends Application{
    
    // Atributos para cambio de nivel
    public String avatar;
    public String user;
    
    @Override
    public void start(Stage stage){
        // Creacion del objeto root
        AnchorPane anchor = new AnchorPane();
        
        // Creacion de boton regresar
        Button btregresar = new Button("Regresar");
        btregresar.setOnAction((ActionEvent event) -> {
            // Tono de Click
            String musicFile = "click.mp3";
            
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
            OptionsWindow optionswindow = new Windows.OptionsWindow();
            optionswindow.setfirstInit(false);
            optionswindow.start(stage);
        });
        
        // Creacion de boton salir
        Button btsalir = new Button("Salir");
        btsalir.setOnAction((ActionEvent t) -> {
            // Tono para dar click
            String musicFile = "click.mp3";
            
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
            salida(stage);
        });
        
        // Label y TextField para el nombre de usuario
        Label lbnombre = new Label("Nombre de Usuario");
        TextField txtnombre = new TextField();
        HBox usuario = new HBox();
        usuario.setSpacing(20);
        
        usuario.getChildren().addAll(lbnombre,txtnombre);
                
        // Imagen de fondo
        Image image = new Image(NewGameWindow.class.getResource("/ResourceGraphics/newgame.jpg").toExternalForm());
        ImageView view = new ImageView();
        view.setImage(image);
        
        // Imagen secundaria
        VBox imagen = new VBox();
        Image image2 = new Image(NewGameWindow.class.getResource("/ResourceGraphics/batalla.gif").toExternalForm());
        ImageView view2 = new ImageView();
        view2.setImage(image2);
        imagen.getChildren().add(view2);
        
        // Creacion de RadioButtons para avatares pollo , vaca
        RadioButton rbcerdo = new RadioButton("Cerdo");
        rbcerdo.setUserData("Cerdo");
        RadioButton rbpollo = new RadioButton("Pollo");
        rbpollo.setUserData("Pollo");
        RadioButton rbvaca = new RadioButton("Vaca");
        rbvaca.setUserData("Vaca");
        Label lbicon = new Label();
        lbicon.setMaxSize(5, 5);
                
        final ToggleGroup group = new ToggleGroup();
        rbcerdo.setToggleGroup(group);
        rbpollo.setToggleGroup(group);
        rbvaca.setToggleGroup(group);
        group.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
            if (group.getSelectedToggle() != null) {
                setAvatar(group.getSelectedToggle().getUserData().toString());
                if (group.getSelectedToggle().getUserData().toString().equals("Vaca") 
                        || group.getSelectedToggle().getUserData().toString().equals("Pollo")) {
                    final Image image1 = new Image(
                            getClass().getResourceAsStream(
                                    group.getSelectedToggle().getUserData().toString() +
                                            ".png"
                            ));
                    lbicon.setGraphic(new ImageView(image1));
                    lbicon.setMaxSize(5, 5);
                } else {
                    final Image image3 = new Image(
                            getClass().getResourceAsStream(
                                    group.getSelectedToggle().getUserData().toString() +
                                            ".png"
                            ));
                    lbicon.setGraphic(new ImageView(image3));
                    lbicon.setMaxSize(5, 5);
                }                
            }
        });
        
        // Texto a mostrar por posible error
        Label error = new Label("");
        error.setFont(new Font("Times New Roman", 20));
        error.setTextFill(Color.RED);
        
        // Layout para la seleccion de personaje
        VBox opcion = new VBox();
        opcion.setSpacing(70);
        opcion.getChildren().addAll(rbcerdo,rbpollo,rbvaca);
        
        // Creacion de boton para iniciar el juego
        Button btiniciar = new Button("Iniciar juego");
        btiniciar.setOnAction((ActionEvent t) -> {
            // Tono de click
            String musicFile = "click.mp3";
            
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
            try{
                if(!txtnombre.getText().equals("")||txtnombre.getText()==null){
                    setUser(txtnombre.getText());
                    String seleccion = group.getSelectedToggle().toString();
                    if(seleccion.contains("Cerdo")){
                        setAvatar("Cerdo");
                        seleccion = "Cerdo";
                    }
                    if(seleccion.contains("Pollo")){
                        setAvatar("Pollo");
                        seleccion = "Pollo";
                    }
                    if(seleccion.contains("Vaca")){                
                        setAvatar("Vaca");
                        seleccion = "Vaca";
                    }
                    
                    PrimerNivel primernivel = new PrimerNivel(avatar, user);
                    primernivel.start(stage);
                }
                else{
                    error.setText("LLENE TODOS LOS CAMPOS!");
                }
                
            }
            catch(NullPointerException ex){
                error.setText("LLENE TODOS LOS CAMPOS! ");
            }
        });
        
        // Donde se guardaran los botones
        HBox botones = new HBox();
        botones.setSpacing(400);
        botones.getChildren().addAll(btregresar, btiniciar, btsalir);
        
        // Orientacion del ingreso de usuario
        AnchorPane.setTopAnchor(usuario, 20.0);
        
        // Orientacion de la opcion los personajes
        AnchorPane.setLeftAnchor(opcion, 5.0);
        AnchorPane.setTopAnchor(opcion, 200.0);
        
        // Orientacion de la imagen del personaje seleccionado
        AnchorPane.setLeftAnchor(lbicon, 150.0);
        AnchorPane.setTopAnchor(lbicon, 250.0);
        
        // Orientacion de los botones del juego
        AnchorPane.setBottomAnchor(botones, 20.0);
        AnchorPane.setLeftAnchor(botones, 100.0);
        
        // Orientacion de la segunda imagen
        AnchorPane.setTopAnchor(imagen, 180.0);
        AnchorPane.setRightAnchor(imagen, 20.0);
        
        // Orientacion del texto a mostrar por posible error
        AnchorPane.setBottomAnchor(error, 60.0);
        AnchorPane.setLeftAnchor(error, 450.0);
              
        anchor.getChildren().addAll(view,opcion, botones,lbicon,usuario,imagen,error);
        
        Scene scene = new Scene(anchor,1180,660);
        stage.setTitle("Ventana de juego nuevo by: eacarras");
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.setScene(scene);

        stage.show();
    }
    
    // Metodo principal para levantar el root
    public static void main(String[] args){
        launch(args);
    }
    
    // Metodo para cuando el usuario quiera salir
    public void salida(Stage stage){
        AnchorPane anchor = new AnchorPane();
        Label mensaje = new Label("¿Desea salir del juego?");
        mensaje.setFont(new Font("Arial",20));
        
        Image image = new Image(NewGameWindow.class.getResource("/ResourceGraphics/fotoSalida.png").toExternalForm());
        ImageView view = new ImageView();
        view.setImage(image);
        
        HBox botones = new HBox();
        Button si = new Button("Si");
        Button no = new Button("No");
        botones.setSpacing(60);
        
        botones.getChildren().addAll(si,no);
        
        AnchorPane.setBottomAnchor(botones, 20.0);
        AnchorPane.setLeftAnchor(botones, 70.0);
        AnchorPane.setTopAnchor(mensaje, 25.0);
        AnchorPane.setLeftAnchor(mensaje, 25.0);
        
        si.setOnAction((ActionEvent event) -> {
            stage.close();
        });
        
        no.setOnAction((ActionEvent event) -> {
            start(stage);
        });
        
        anchor.getChildren().addAll(view, mensaje, botones);
        
        Scene scene = new Scene(anchor,260,159);
        
        stage.setTitle("Aviso!");
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }
    
    // Metodo de acceso
    public void setAvatar(String personaje){
        this.avatar = personaje;
    }
    
    public void setUser(String user) {
        this.user = user;
    }
}
