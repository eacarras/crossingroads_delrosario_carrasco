
package Windows;

import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

/**
 *
 * @author eacarras
 */
public class Carros{
    public double SCENE_WIDTH;
    public double SCENE_HEIGHT;
    public double BALL_RADIUS;
    public double ballSpeed;

    public Group animationCar(Stage primaryStage) {
        Group root = new Group();

        // Bola que se usará para la animación
        Circle ball = new Circle(BALL_RADIUS);
        ball.setTranslateX(SCENE_WIDTH * 0.5);
        ball.setTranslateY(SCENE_HEIGHT * 0.5);
        root.getChildren().addAll(ball);

        // Game loop usando AnimationTimer
        AnimationTimer animationTimer = new AnimationTimer() {
            public void handle(long now) {
                // Cambiar la dirección de la bola si llega a los extremos
                if(ball.getTranslateX() < 0 || ball.getTranslateX() > SCENE_WIDTH) {
                    ballSpeed *= -1;
                }                
                ball.setTranslateX(ball.getTranslateX() + ballSpeed);
            }
        };
        animationTimer.start();
        return root;
    }
}
