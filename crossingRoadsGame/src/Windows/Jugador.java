
package Windows;

/**
 *
 * @author msdrc2402
 */
public class Jugador {
    protected String usuario;
    protected String tiempo;
    protected String monedas;
    protected String fecha;
    protected String avatar;

    public Jugador(String usuario, String tiempo, String monedas, String fecha, String avatar) {
        this.usuario = usuario;
        this.tiempo = tiempo;
        this.monedas = monedas;
        this.fecha = fecha;
        this.avatar = avatar;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getTiempo() {
        return tiempo;
    }

    public String getMonedas() {
        return monedas;
    }

    public String getFecha() {
        return fecha;
    }

    public String getAvatar() {
        return avatar;
    }
}
