
package Windows;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Date;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author eacarras
 */
public class NivelTerminado extends Application{
    
    // Atributos para guardar informacion
    final private String user;
    final private String avatar;
    final private int monedas;
    final private int tiempo;
    final private String fecha = obtenerFecha();
    
    // Atributo del archivo a guardar
    final private String file = "src/Files/historial.txt";
    
    public NivelTerminado(String user, String avatar, int monedas, int tiempo) {
        this.user = user;
        this.avatar = avatar;
        this.monedas = monedas;
        this.tiempo = tiempo;
    }
    
    @Override
    public void start(Stage stage){
        // Guardar data
        guardarData();
        
        // Todo el resto de logica
        AnchorPane anchorpane =  new AnchorPane();
        
        // Imagen de fondo
        Image image = new Image(OptionsWindow.class.getResource("/ResourceGraphics/fondoganador.gif").toExternalForm());
        ImageView fondo = new ImageView(image);
            
            // Boton de salir
            Button btSalir = new Button("Salir");
            btSalir.setOnAction((ActionEvent event) -> {
                // Tono de Click
                String musicFile = "click.mp3";
                
                Media sound = new Media(new File(musicFile).toURI().toString());
                MediaPlayer mediaPlayer = new MediaPlayer(sound);
                mediaPlayer.play();
                // Cierra el programa
                stage.close();
        });
            
            // Boton de volver al menu principal
            Button btMenuPrincipal = new Button("Jugar de Nuevo");
            btMenuPrincipal.setOnAction((ActionEvent event) -> {
                // Tono de Click
                String musicFile = "click.mp3";
                
                Media sound = new Media(new File(musicFile).toURI().toString());
                MediaPlayer mediaPlayer = new MediaPlayer(sound);
                mediaPlayer.play();
                // Ir a jugar de nuevo
                OptionsWindow optionwindow = new OptionsWindow();
                optionwindow.setfirstInit(false);
                optionwindow.start(stage);
        });
            
            HBox botones = new HBox();
            botones.setSpacing(120);
            botones.getChildren().addAll(btMenuPrincipal,btSalir);
            
            AnchorPane.setBottomAnchor(botones, 20.0);
            AnchorPane.setLeftAnchor(botones, 120.0);
            
            // Label con informacion para presentar 
            Label lbMensaje = new Label("Has ganado!! , Te "
                    + "invitamos a jugar nuevamente...");
            lbMensaje.setFont(new Font("Times New Roman",22));
            lbMensaje.setTextFill(Color.YELLOW);
            AnchorPane.setTopAnchor(lbMensaje, 15.0);
            AnchorPane.setLeftAnchor(lbMensaje, 10.0);
            AnchorPane.setRightAnchor(lbMensaje, 50.0);
            
            anchorpane.getChildren().addAll(fondo,lbMensaje, botones);
            
            // Setear ambiente para correr
            Scene scene = new Scene(anchorpane,502 ,275);
        
            stage.setScene(scene);
            stage.setTitle("Mensaje de Felicidades by: eacarras");
            stage.setResizable(false);
            stage.show();
    }
    
    public static void main(String[] args){
        launch(args);
    }
    
    public String obtenerFecha() {
        Date date = new Date();
        return date.toString();
    }
    
    private void guardarData() {
        String informacion;
        informacion = user+","+Integer.toString(tiempo)+","+
                Integer.toString(monedas)+","+fecha+","+avatar;
        try{
            System.out.println("Los datos se ha guardado correctamente");
            File archivo = new File(file);
            BufferedWriter buffer = new BufferedWriter(new FileWriter(archivo));
            buffer.write(informacion+"\n");
            buffer.close();
        }catch(Exception e){
            System.out.println("Error al guardar data: " + e);
        }                
    }
}
