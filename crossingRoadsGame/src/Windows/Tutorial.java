
package Windows;

import java.io.File;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author msdrc2402
 */
public class Tutorial extends Application{
    
    @Override
    public void start(Stage stage){
        AnchorPane anchor = new AnchorPane();
        
        // Imagen de fondo
        Image image = new Image(OptionsWindow.class.getResource("/ResourceGraphics/fondoTutorial2.jpg").toExternalForm());
        ImageView fondo = new ImageView(image);
        
        // Manejo del personaje
        Label manejo = new Label("Para el manejo del personaje, use las teclas "
                                 + "direccionales de su teclado.");
        manejo.setFont(new Font("Times New Roman",20));
        manejo.setTextFill(Color.YELLOW);
        
        Image image1 = new Image(OptionsWindow.class.getResource("/ResourceGraphics/direccionales.png").toExternalForm());
        ImageView direccionales = new ImageView(image1);
        
        // Objetivo del juego
        VBox objetivos = new VBox();
        objetivos.setSpacing(5);
        Label objetivo = new Label("Para poder avanzar en los niveles tiene que:");
        Label objetivo1 = new Label("1. cruzar todas las avenidas antes de que acabe el juego.");
        Label objetivo2 = new Label("2. coleccionar las monedas generadas para "
                                 + "avanzar al siguiente nivel");
        Label objetivo3 = new Label("3. ganar en el menor tiempo posible y quedar "
                                 + "en el tablero de la fama!");
        
        objetivo.setFont(new Font("Times New Roman",20));
        objetivo1.setFont(new Font("Times New Roman",20));
        objetivo2.setFont(new Font("Times New Roman",20));
        objetivo3.setFont(new Font("Times New Roman",20));
        
        objetivo.setTextFill(Color.YELLOW);
        objetivo1.setTextFill(Color.YELLOW);
        objetivo2.setTextFill(Color.YELLOW);
        objetivo3.setTextFill(Color.YELLOW);
        
        objetivos.getChildren().addAll(objetivo,objetivo1,objetivo2,objetivo3);
        
        Image image2 = new Image(OptionsWindow.class.getResource("/ResourceGraphics/juego.png").toExternalForm());
        ImageView tutorialJuego = new ImageView(image2);
        
        // Boton regresar
        Button regresar = new Button("Regresar");
        
        regresar.setOnAction((ActionEvent event) -> {
            // Tono para dar click
            String musicFile = "click.mp3";
            
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
            // Cambio a la pagina de opciones
            OptionsWindow option = new OptionsWindow();
            option.setfirstInit(false);
            option.start(stage);
        });
        
        regresar.setStyle("-fx-background-color: YELLOW;");
        

        // Ubicacion del primer texto con su respectiva imagen
        AnchorPane.setTopAnchor(manejo, 20.0);
        AnchorPane.setLeftAnchor(manejo, 10.0);
        
        AnchorPane.setTopAnchor(direccionales, 50.0);
        AnchorPane.setRightAnchor(direccionales, 300.0);
        
        // Ubicacion de los objetivos
        AnchorPane.setTopAnchor(objetivos, 220.0);
        AnchorPane.setLeftAnchor(objetivos, 10.0);
         
        AnchorPane.setTopAnchor(tutorialJuego, 350.0);
        AnchorPane.setRightAnchor(tutorialJuego, 300.0);
        
        // Ubicacion del boton
        AnchorPane.setBottomAnchor(regresar, 40.0);
        AnchorPane.setRightAnchor(regresar, 380.0);
        
        anchor.getChildren().addAll(fondo,manejo, direccionales, objetivos, tutorialJuego, regresar);
        
        Scene scene = new Scene(anchor,800,600);
        stage.setTitle("Tutorial by:msdrc2402");
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
        
    }
    
    public void main (String[] args){
        launch(args);
    }
    
}
