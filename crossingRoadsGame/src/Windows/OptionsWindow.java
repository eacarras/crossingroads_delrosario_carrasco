package Windows;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;


/**
 *
 * @author eacarras && msdrc
 */
public class OptionsWindow extends Application{
    
    // Atributos para mostrar el historial si este existiera
    final private String file = "src/Files/historial.txt";
    private boolean history = false;
    
    // Atributo para que no se repita la cancion
    private boolean firstInit = true;
    
    @Override
    public void start(Stage stage){
        // Verificar existencia de historial
        verificarHistory();
        
        // Creacion de objeto root
        AnchorPane root = new AnchorPane();
        
        //uso de un Hbox para los botones
        HBox botones = new HBox();
        botones.setSpacing(55);
        
        // Creacion del boton menú
        Button btnuevojuego = new Button("Nuevo Juego");
        btnuevojuego.setOnAction((ActionEvent event) -> {
            System.out.println("Bienvenido jugador..");
            NewGameWindow newgamewindow = new Windows.NewGameWindow();
            newgamewindow.start(stage);
        });
        
        //Creacion del boton de Historial de Jugadores
        Button bthistorial = new Button("Historial de Jugadores");
        bthistorial.setOnAction((ActionEvent event) -> {
            // Tono para dar click
            String musicFile = "click.mp3";
            
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
            
            // Validacion si existe ganadores
            if(history){
                try {
                    tablaJugador(stage);
                }catch(Exception exception){
                    System.out.println("El error es: " + exception);
                    System.exit(1);
                }
            }else{
                // Mostrar ventana emergente
                ventanaEmergente(stage);
            }
        });        
        
        // Creacion de boton salir 
        Button btsalir = new Button("Salir");
        btsalir.setOnAction((ActionEvent event) -> {
            // Tono para dar click
            String musicFile = "click.mp3";
            
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
            // Cierra la app
            salida(stage);
        });
        
        //Creacion del boton guia
        Button btguia = new Button("Instrucciones");
        btguia.setOnAction((ActionEvent event) -> {
            // Tono para dar click
            String musicFile = "click.mp3";
            
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
            
            // Abrir el tutorial
            Tutorial tutorial = new Tutorial();
            tutorial.start(stage);
        });
        
        // Imagen de fondo de pantalla
        Image image = new Image(OptionsWindow.class.getResource("/ResourceGraphics/wallpaper.jpg").toExternalForm());
        ImageView view = new ImageView();
        view.setImage(image);
        
        // Guardado de los botones en el HBox
        botones.getChildren().addAll(bthistorial,btnuevojuego,btguia,btsalir);
        botones.setPadding(new Insets(20));
        
        // Ubicacion de los botones
        AnchorPane.setBottomAnchor(botones, 10.0);  
        // Agregado de las variables al layout principal
        root.getChildren().addAll(view, botones);
        
        // Agregar musica
        if (firstInit){
            String musicFile = "InicioDelJuego.mp3";

            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
            mediaPlayer.play();
            setfirstInit(false);
        } 
        
        // Setear valores para imprimirlos
        Scene scene = new Scene(root,540,275);
        stage.setTitle("Crossing Roads eacarras/msdrc");
        stage.setResizable(false);
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.show();        
    }
    
    
    // Metodo principal para levantar el root
    public void main (String[] args){
        launch(args);
    }
    
    // Ventana emergente
    public void ventanaEmergente(Stage stage){
        AnchorPane anchorpane =  new AnchorPane();
        
        //imagen de fondo
        Image image = new Image(OptionsWindow.class.getResource("/ResourceGraphics/fondoHistorial.jpg").toExternalForm());
        ImageView fondo = new ImageView(image);
        
        // Mensaje emergente
        Label lbmensaje = new Label("No se puede cargar los archivos"
                    + " porque no existen ganadores aun...");
        lbmensaje.setFont(new Font("Times New Roman",20));
        lbmensaje.setTextFill(Color.YELLOW);
        
        AnchorPane.setTopAnchor(lbmensaje, 15.0);
        AnchorPane.setLeftAnchor(lbmensaje, 10.0);
        AnchorPane.setRightAnchor(lbmensaje, 50.0);
        // Boton de salir
        Button btsalir = new Button("Regresar");
        AnchorPane.setBottomAnchor(btsalir, 25.0);
        AnchorPane.setLeftAnchor(btsalir, 160.0);
        AnchorPane.setRightAnchor(btsalir, 160.0);
        btsalir.setOnAction((ActionEvent event) -> {
            // Tono para click
            String musicFile = "click.mp3";
            
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
            stage.close();
            start(stage);
        });
        anchorpane.getChildren().addAll(fondo,lbmensaje, btsalir);
        // Setear ambiente para correr
        Scene scene = new Scene(anchorpane,800 ,400);
        
        stage.setTitle("AnchorPane Layout by: eacarras");
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }
    
    @Override
    public void stop(){      
        System.out.println("Vuelva pronto..");
    }
    
    // Metodo para verificar que el usuario quiera salir
    public void salida(Stage stage){
        AnchorPane anchor = new AnchorPane();
        Label mensaje = new Label("¿Desea salir del juego?");
        mensaje.setFont(new Font("Arial",20));
        
        Image image = new Image(OptionsWindow.class.getResource("/ResourceGraphics/fotoSalida.png").toExternalForm());
        ImageView view = new ImageView();
        view.setImage(image);
        
        HBox botones = new HBox();
        Button si = new Button("Si");
        Button no = new Button("No");
        botones.setSpacing(60);
        
        botones.getChildren().addAll(si,no);
        
        AnchorPane.setBottomAnchor(botones, 20.0);
        AnchorPane.setLeftAnchor(botones, 70.0);
        AnchorPane.setTopAnchor(mensaje, 25.0);
        AnchorPane.setLeftAnchor(mensaje, 25.0);
        
        si.setOnAction((ActionEvent event) -> {
            // Tono de click
            String musicFile = "click.mp3";
            
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
            stage.close();
        });
        
        no.setOnAction((ActionEvent event) -> {
            // Tono de click
            String musicFile = "click.mp3";
            
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
            start(stage);
        });
        
        anchor.getChildren().addAll(view, mensaje, botones);
        
        Scene scene = new Scene(anchor,260,159);
        
        stage.setTitle("Aviso!");
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }
    
    // Metodo que muestra la tabla de jugadores que han ganado
    public void tablaJugador(Stage stage){
        TableView<Jugador> tabla = new TableView<>();
        Label label = new Label("JUGADORES GANADORES");
        label.setFont(new Font("Arial",20));
        
        tabla.setEditable(false);
        
        // Setear valores para presentar informacion en las columnas
        TableColumn<Jugador, String> usuario = new TableColumn<>("Usuario");
        TableColumn<Jugador, String> tiempo = new TableColumn<>("Tiempo");
        TableColumn<Jugador, String> monedas = new TableColumn<>("Monedas");
        TableColumn<Jugador, String> fecha = new TableColumn<>("Fecha");
        TableColumn<Jugador, String> avatar = new TableColumn<>("Avatar");
        
        tabla.getColumns().addAll(usuario,tiempo,monedas,fecha,avatar);
        
        usuario.setCellValueFactory(new PropertyValueFactory<>("usuario"));
        tiempo.setCellValueFactory(new PropertyValueFactory<>("tiempo"));
        monedas.setCellValueFactory(new PropertyValueFactory<>("monedas"));
        fecha.setCellValueFactory(new PropertyValueFactory<>("fecha"));
        avatar.setCellValueFactory(new PropertyValueFactory<>("avatar"));
        
        // Mostrar data en el tableView
        try{
            System.out.println("Mostrando los jugadores ganadores");
            String cadena;
            FileReader archivo = new FileReader(file);
            BufferedReader buffer = new BufferedReader(archivo);
            while((cadena = buffer.readLine()) != null){
                String[] array = cadena.split(",");
                Jugador jugador = new Jugador(array[0],array[1],
                        array[2],array[3],array[4]);
                tabla.getItems().addAll(jugador);
            }
            buffer.close();
        } catch(Exception e) {
            System.out.println("Error al tratar de cargar los ganadores" + e);
        }
        
        // Resto de logica del tableView
        
        Button regresar = new Button("Regresar");
        
        regresar.setOnAction((ActionEvent event) -> {
            // Tono de click
            String musicFile = "click.mp3";
            
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
            start(stage);
        });
        
        AnchorPane anchor = new AnchorPane();
        
        AnchorPane.setTopAnchor(label, 20.0);
        AnchorPane.setLeftAnchor(label, 120.0);
        
        AnchorPane.setTopAnchor(tabla, 60.0);
        AnchorPane.setLeftAnchor(tabla, 55.0);
        
        AnchorPane.setBottomAnchor(regresar, 10.0);
        AnchorPane.setLeftAnchor(regresar, 225.0);
        
        anchor.getChildren().addAll(label,tabla,regresar);
        
        anchor.setStyle("-fx-background-color: AQUAMARINE;");
        Scene scene = new Scene(anchor,500,500);
        stage.setTitle("Tabla de jugadores, by: msdrc2402");

        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
        
    }
    
    // Verificar si hay data en el txt
    public void verificarHistory() {
        try{
            System.out.println("Historial cargado correctamente");
            String cadena;
            FileReader archivo = new FileReader(file);
            BufferedReader buffer = new BufferedReader(archivo);
            while((cadena = buffer.readLine()) != null){
                String[] array = cadena.split(",");
                if (array.length > 1) {
                    setHistory(true);
                }
            }
            buffer.close();
        } catch(Exception e) {
            System.out.println("Error al trata de verificar el historial" + e);
        }
        
    }
       
    // Metodos de acceso al atributo history
    public void setHistory(boolean value){
        this.history = value;
    }
    
    public void setfirstInit(boolean value){
        this.firstInit = value;
    }
}
