
package Windows;

import java.io.File;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author eacarras
 */
public class JuegoTerminado extends Application{
    
    @Override
    public void start(Stage stage){
        AnchorPane anchor = new AnchorPane();
        
        // Imagen de fondo
        Image image = new Image(OptionsWindow.class.getResource("/ResourceGraphics/fondoperdido.jpg").toExternalForm());
        ImageView fondo = new ImageView(image);
        
        Label mensaje = new Label("Has perdido :(,"
                + "inténtalo nuevamente");
        mensaje.setFont(new Font("Arial",20));
        mensaje.setTextFill(Color.YELLOW);

        HBox botones = new HBox();
        Button salir = new Button("Salir");
        Button volver = new Button("Volver");
        botones.setSpacing(80);
        
        botones.getChildren().addAll(salir, volver);
        
        AnchorPane.setBottomAnchor(botones, 10.0);
        AnchorPane.setLeftAnchor(botones, 160.0);
        AnchorPane.setTopAnchor(mensaje, 15.0);
        AnchorPane.setLeftAnchor(mensaje, 75.0);
        
        salir.setOnAction((ActionEvent event) -> {
            // Tono de Click
            String musicFile = "click.mp3";
            
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
            // Salir de la aplicacion
            stage.close();
        });
        
        volver.setOnAction((ActionEvent event) -> {
            // Tono de Click
            String musicFile = "click.mp3";
            
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
            // Ir a menu principal
            NewGameWindow newgame = new NewGameWindow();
            newgame.start(stage);
        });
        
        anchor.getChildren().addAll(fondo,mensaje, botones);
        
        Scene scene = new Scene(anchor,470,350);
        
        stage.setTitle("Ventana Game Over by: eacarras");
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }
    
    // Metodo principal que levanta la app
    public static void main(String[] args){
        launch(args);
    }
}
