
package Windows;

import java.io.File;
import java.text.DecimalFormat;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.WindowEvent;

/**
 *
 * @author eacarras
 */
public class PrimerNivel extends Application{
    
    // Atributo para mover al avatar
    private static final int KEYBOARD_MOVEMENT = 5;
    
    // Atributo para ejecutar el relog
    final private DigitalClock clock = new DigitalClock();
    private boolean contando = false;
    
    // Atributo para informacion del usuario
    public String avatar;
    public String user;
    final public int monedas = 1;
    
    // Atributo para poner el tamaño de la scene
    private final static double SCENE_WIDTH = 700;
    private final static double SCENE_HEIGHT = 390;
    
    // Velocidad de balon
    private static double ballSpeed = 2;
    private static double ballSpeedtwo = 2;
    private static double ballSpeedthree = 2;
    private static double ballSpeedfour = 2;
    private static double ballSpeedfive = 2;
    
    public ImageView carro1;
    public ImageView carro2;
    public ImageView carro3;
    public ImageView carro4;
    public ImageView carro5;
    
    public ImageView corazon1;
    public ImageView corazon2;
    public ImageView corazon3;
    public ImageView corazonRoto;
    public ImageView moneda;
    
    // Atributos para cambio de nivel
    public int vidas = 3;
    private int segundos;
    final private double lugar = 340.0;
    final private JuegoTerminado juegoTerminado = new JuegoTerminado();
    
    // Constructor para mandarle el avatar a usar
    public PrimerNivel (String avatar, String user) {
        this.avatar = avatar;
        this.user = user;
    }
    
    @Override
    public void start(Stage stage){
        // Objeto donde estara el personaje por el momento un circulo
        Image image0 = new Image(OptionsWindow.class.getResource("/ResourceGraphics/"+avatar+".png").toExternalForm());
        ImageView avatar = new ImageView(image0);
        avatar.setFitWidth(30);
        avatar.setFitHeight(30);
        
        // Imagen de fondo
        Image image = new Image(OptionsWindow.class.getResource("/ResourceGraphics/fondonivel1.jpg").toExternalForm());
        ImageView fondo = new ImageView(image);
        
        // Creacion del primer carro
        Image image1 = new Image(OptionsWindow.class.getResource("/ResourceGraphics/carro1.png").toExternalForm());
        carro1 = new ImageView(image1);
        carro1.setFitHeight(50);
        carro1.setFitWidth(70);
        //Circle carro1 = new Circle(BALL_RADIUS);
        carro1.setTranslateX(0);
        carro1.setTranslateY(45);

        // Game loop usando AnimationTimer
        AnimationTimer animationTimer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                // Cambiar la dirección de la bola si llega a los extremos
                if(carro1.getTranslateX() < 0 || carro1.getTranslateX() > SCENE_WIDTH) {
                    ballSpeed *= -1;
                }                
                carro1.setTranslateX(carro1.getTranslateX() + ballSpeed);
            }
        };
        animationTimer.start();
        
        // Creacion del segundo carro
        Image image2 = new Image(OptionsWindow.class.getResource("/ResourceGraphics/carro2.png").toExternalForm());
        carro2 = new ImageView(image2);
        carro2.setFitHeight(50);
        carro2.setFitWidth(70);
        //Circle carro1 = new Circle(BALL_RADIUS);
        carro2.setTranslateX(SCENE_WIDTH * 0.5);
        carro2.setTranslateY(30);

        // Game loop usando AnimationTimer
        AnimationTimer animationTimertwo = new AnimationTimer() {
            @Override
            public void handle(long now) {
                // Cambiar la dirección de la bola si llega a los extremos
                if(carro2.getTranslateX() < 0 || carro2.getTranslateX() > SCENE_WIDTH) {
                    ballSpeedtwo *= -1;
                }                
                carro2.setTranslateX(carro2.getTranslateX() + ballSpeedtwo);
            }
        };
        animationTimertwo.start();
        
        // Creacion del tercer carro
        Image image3 = new Image(OptionsWindow.class.getResource("/ResourceGraphics/carro3.png").toExternalForm());
        carro3 = new ImageView(image3);
        carro3.setFitHeight(50);
        carro3.setFitWidth(70);
        carro3.setTranslateX(SCENE_WIDTH * 0.5);
        carro3.setTranslateY(70);

        // Game loop usando AnimationTimer
        AnimationTimer animationTimerthree = new AnimationTimer() {
            @Override
            public void handle(long now) {
                // Cambiar la dirección de la bola si llega a los extremos
                if(carro3.getTranslateX() < 0 || carro3.getTranslateX() > SCENE_WIDTH) {
                    ballSpeedthree *= -1;
                }                
                carro3.setTranslateX(carro3.getTranslateX() + ballSpeedthree);
            }
        };
        animationTimerthree.start();
        
        // Creacion del cuarto carro
        Image image4 = new Image(OptionsWindow.class.getResource("/ResourceGraphics/carro4.jpg").toExternalForm());
        carro4 = new ImageView(image4);
        carro4.setFitHeight(50);
        carro4.setFitWidth(70);
        carro4.setTranslateX(SCENE_WIDTH * 0.5);
        carro4.setTranslateY(90);

        // Game loop usando AnimationTimer
        AnimationTimer animationTimerfour = new AnimationTimer() {
            @Override
            public void handle(long now) {
                // Cambiar la dirección de la bola si llega a los extremos
                if(carro4.getTranslateX() < 0 || carro4.getTranslateX() > SCENE_WIDTH) {
                    ballSpeedfour *= -1;
                }                
                carro4.setTranslateX(carro4.getTranslateX() + ballSpeedfour);
            }
        };
        animationTimerfour.start();
        
        // Creacion del quinto carro
        Image image5 = new Image(OptionsWindow.class.getResource("/ResourceGraphics/carro5.png").toExternalForm());
        carro5 = new ImageView(image5);
        carro5.setFitHeight(50);
        carro5.setFitWidth(70);
        carro5.setTranslateX(0);
        carro5.setTranslateY(120);

        // Game loop usando AnimationTimer
        AnimationTimer animationTimerfive = new AnimationTimer() {
            @Override
            public void handle(long now) {
                // Cambiar la dirección de la bola si llega a los extremos
                
                if(carro5.getTranslateX() < 0 || carro5.getTranslateX() > SCENE_WIDTH) {
                    ballSpeedfive *= -1;
                }                
                carro5.setTranslateX(carro5.getTranslateX() + ballSpeedfive);
            }
        };
        animationTimerfive.start();
       
        //imagenes de las vidas y monedas
        Image image6 = new Image(OptionsWindow.class.getResource("/ResourceGraphics/corazon.png").toExternalForm());
        corazon1 = new ImageView(image6);
        corazon1.setFitWidth(60);
        corazon1.setFitHeight(60);
        
        Image image9 = new Image(OptionsWindow.class.getResource("/ResourceGraphics/corazon.png").toExternalForm());
        corazon2 = new ImageView(image9);
        corazon2.setFitWidth(60);
        corazon2.setFitHeight(60);
        Image image10 = new Image(OptionsWindow.class.getResource("/ResourceGraphics/corazon.png").toExternalForm());
        corazon3 = new ImageView(image10);
        corazon3.setFitWidth(60);
        corazon3.setFitHeight(60);
        
        Image image7 = new Image(OptionsWindow.class.getResource("/ResourceGraphics/corazonRoto.png").toExternalForm());
        corazonRoto = new ImageView(image7);
        corazonRoto.setFitWidth(60);
        corazonRoto.setFitHeight(60);
        
        Label lbmoneda = new Label("MONEDAS:");
        lbmoneda.setFont(new Font("Times New Roman", 20));
        
        Image image8 = new Image(OptionsWindow.class.getResource("/ResourceGraphics/moneda.png").toExternalForm());
        moneda = new ImageView(image8);
        moneda.setFitWidth(60);
        moneda.setFitHeight(60);
        

        
        AnchorPane anchor = new AnchorPane();
        
        AnchorPane.setTopAnchor(clock,5.0);
        AnchorPane.setLeftAnchor(clock,5.0);
        
        AnchorPane.setTopAnchor(corazon1,5.0);
        AnchorPane.setRightAnchor(corazon1, 5.0);
        AnchorPane.setTopAnchor(corazon2,5.0);
        AnchorPane.setRightAnchor(corazon2, 65.0);
        AnchorPane.setTopAnchor(corazon3,5.0);
        AnchorPane.setRightAnchor(corazon3, 125.0);
        
        AnchorPane.setTopAnchor(moneda, 5.0);
        AnchorPane.setLeftAnchor(moneda, 200.0);
        AnchorPane.setTopAnchor(lbmoneda, 25.0);
        AnchorPane.setLeftAnchor(lbmoneda, 100.0);
        
        AnchorPane.setTopAnchor(clock,2.5);
       
        //carro 1
        AnchorPane.setTopAnchor(carro1, 65.0);
        AnchorPane.setLeftAnchor(carro1, 40.0);
        //carro 2: sale
        AnchorPane.setTopAnchor(carro2, 80.0);
        AnchorPane.setLeftAnchor(carro2, 1.0);
        //bola 3: sale
        AnchorPane.setTopAnchor(carro3, 225.0);
        AnchorPane.setLeftAnchor(carro3, 50.0);
        //bola 4: sale
        AnchorPane.setTopAnchor(carro4, 110.0);
        AnchorPane.setLeftAnchor(carro4, 90.0);
        //bola 5
        AnchorPane.setTopAnchor(carro5, 80.0);
        AnchorPane.setLeftAnchor(carro5, 10.0);
    
        anchor.getChildren().addAll(fondo,clock,corazon1,corazon2,corazon3,lbmoneda,moneda,avatar,carro1,carro2,carro3,carro4,carro5);
        
        final Scene scene = new Scene(anchor, SCENE_WIDTH, SCENE_HEIGHT, 
                Color.CORNSILK);
        moveCircleOnKeyPress(scene, avatar, stage);
                
        // Scene scene = new Scene(gridpane ,950 ,400);
        stage.setScene(scene);
        stage.setTitle("Primer Nivel by: eacarras/msdrc2402");
        stage.setResizable(false);
        stage.centerOnScreen();
        stage.show();
        stage.setOnCloseRequest((WindowEvent t) -> {
            // Setear variables para que vuelva a iniciar
            contando = false;
            stage.close();
        });
        iniciarRelog();
    }
    
    // Metodo main
    public static void main(String[] args){
        launch(args);
    }
    
    private void iniciarRelog(){
         contando = true;
         new Thread(){
             @Override
             public void run(){
                 long last = System.nanoTime();
                 double delta = 0;
                 double ns = 1000000000.0 / 1;
                 int count = 0;
                 
                 while(contando){
                     long now = System.nanoTime();
                     delta += (now - last) / ns;
                     last = now;
                     
                     while(delta >= 1){
                         count = (count + 1) % 60;
                         setSegundos(count);
                         DecimalFormat decimalformat = new DecimalFormat("00");
                         clock.refrescarDigitos(decimalformat.format(count));
                         if (count == 59){
                             stop();
                         }
                         delta-- ;
                     }
                 }
                 
             }
         }.start();
    }
    
    // Thread para mostrar las vidas
    private void Vidas() {
        new Thread() {
            @Override
            public void run() {
                while(contando) {
                    switch (vidas) {                    
                        case 3:
                            // Presentar 3 vidas
                            break;                    
                        case 2:
                            // Presentar 2 vidas
                            break;                    
                        default:
                            // Presentar una vida
                            break;
                    }
                }
            }
        }.start();
    }
    
    private void moveCircleOnKeyPress(Scene scene, ImageView avatar, 
            Stage stage) {
        scene.setOnKeyPressed((KeyEvent event) -> {
            switch (event.getCode()) {
                case UP:    avatar.setY(avatar.getY()- KEYBOARD_MOVEMENT);
                if (crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro1)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro2)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro3)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro4)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro5)){
                    // Musica de Choque
                    String musicFile = "Choque.mp3";
                    
                    Media sound = new Media(new File(musicFile).toURI().toString());
                    MediaPlayer mediaPlayer = new MediaPlayer(sound);
                    mediaPlayer.play();
                    
                    if (getVidas() == 0) {
                        juegoTerminado.start(stage);
                    }
                    setVidas(getVidas() - 1);
                } else if (avatar.getY() == lugar) {
                    SegundoNivel segundo = new SegundoNivel(getAvatar(), getUser(),
                            getMonedas() + 1, getSegundos());
                    segundo.start(stage);
                } else if (getSegundos() == 59 ) {
                    juegoTerminado.start(stage);
                }
                
                
                
                
                break;
                case RIGHT: avatar.setX(avatar.getX() + KEYBOARD_MOVEMENT);
                if (crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro1)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro2)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro3)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro4)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro5)){
                    // Musica de Choque
                    String musicFile = "Choque.mp3";
                    
                    Media sound = new Media(new File(musicFile).toURI().toString());
                    MediaPlayer mediaPlayer = new MediaPlayer(sound);
                    mediaPlayer.play();
                    
                    if (getVidas() == 0) {
                        juegoTerminado.start(stage);
                    }
                    setVidas(getVidas() - 1);
                } else if(avatar.getY() == lugar) {
                    SegundoNivel segundo = new SegundoNivel(getAvatar(), getUser(),
                            getMonedas() + 1, getSegundos());
                    segundo.start(stage);
                } else if (getSegundos() == 59 ) {
                    juegoTerminado.start(stage);
                }
                
                
                break;
                
                case DOWN:  avatar.setY(avatar.getY() + KEYBOARD_MOVEMENT);
                if (crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro1)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro2)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro3)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro4)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro5)){
                    // Musica de Choque
                    String musicFile = "Choque.mp3";
                    
                    Media sound = new Media(new File(musicFile).toURI().toString());
                    MediaPlayer mediaPlayer = new MediaPlayer(sound);
                    mediaPlayer.play();
                    
                    if (getVidas() == 0) {
                        juegoTerminado.start(stage);
                    }
                    setVidas(getVidas() - 1);
                } else if(avatar.getY() == lugar) {
                    SegundoNivel segundo = new SegundoNivel(getAvatar(), getUser(),
                            getMonedas() + 1, getSegundos());
                    segundo.start(stage);
                } else if (getSegundos() == 59 ) {
                    juegoTerminado.start(stage);
                }
                
                
                
                break;
                case LEFT:  avatar.setX(avatar.getX() - KEYBOARD_MOVEMENT);
                if (crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro1)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro2)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro3)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro4)
                        ||crossingroadsgame.CrossingRoadsGame.Choque(avatar, carro5)){
                    // Musica de Choque
                    String musicFile = "Choque.mp3";
                    
                    Media sound = new Media(new File(musicFile).toURI().toString());
                    MediaPlayer mediaPlayer = new MediaPlayer(sound);
                    mediaPlayer.play();
                    
                    if (getVidas() == 0) {
                        juegoTerminado.start(stage);
                    }
                    setVidas(getVidas() - 1);
                } else if(avatar.getY() == lugar) {
                    SegundoNivel segundo = new SegundoNivel(getAvatar(), getUser(),
                            getMonedas() + 1, getSegundos());
                    segundo.start(stage);
                } else if (getSegundos() == 59 ) {
                    juegoTerminado.start(stage);
                }
                
                
                break;
            }
        });
    }
    
    // Metodos de Acceso   
    public String getAvatar() {
        return this.avatar;
    }
    
    public void setVidas(int vidas){
        this.vidas = vidas;
    }
    
    public int getVidas() {
        return this.vidas;
    }
    
    public void setSegundos(int segundos) {
        this.segundos = segundos;
    }
    
    public int getSegundos() {
        return this.segundos;
    }
    
    public int getMonedas() {
        return this.monedas;
    }
    
    public String getUser() {
        return this.user;
    }
}
    

